import 'package:test_task/core/extensions/list_extensions.dart';
import 'package:test_task/features/transactions/data/data_sources/remote/i_remote_transactions_data_source.dart';
import 'package:test_task/features/transactions/data/models/transaction_model.dart';

final class MockRemoteTransactionsDataSource implements IRemoteTransactionsDataSource {
  final _transactions = [
    TransactionModel(
      type: 'transfer',
      date: '9.9.2023',
      number: '1111',
      amount: 9,
      fee: 1,
      total: 8,
    ),
    TransactionModel(
      type: 'deposit',
      date: '10.9.2023',
      number: '2222',
      amount: 10,
      fee: 1.1,
      total: 8.9,
    ),
    TransactionModel(
      type: 'withdrawal',
      date: '11.9.2023',
      number: '3333',
      amount: 11,
      fee: 1.2,
      total: 9.8,
    ),
    TransactionModel(
      type: 'random',
      date: '12.9.2023',
      number: '4444',
      amount: 12,
      fee: 1.3,
      total: 10.7,
    ),
  ];

  @override
  Future<List<TransactionModel>> fetchTransactions() async {
    await Future.delayed(const Duration(milliseconds: 1500));
    return _transactions;
  }

  @override
  Future<bool> removeTransactions(String number) async {
    await Future.delayed(const Duration(milliseconds: 1500));
    final item = _transactions.firstWhereOrNull((element) => element.number == number);
    if (item == null) return false;
    return _transactions.remove(item);
  }
}
