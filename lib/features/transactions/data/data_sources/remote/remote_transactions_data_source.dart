

import 'package:test_task/features/transactions/data/data_sources/remote/i_remote_transactions_data_source.dart';
import 'package:test_task/features/transactions/data/models/transaction_model.dart';

final class RemoteTransactionsDataSource implements IRemoteTransactionsDataSource {
  @override
  Future<List<TransactionModel>> fetchTransactions() {
    // TODO: implement fetchTransactions
    throw UnimplementedError();
  }

  @override
  Future<bool> removeTransactions(String number) {
    // TODO: implement removeTransactions
    throw UnimplementedError();
  }
}
