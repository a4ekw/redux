import 'package:test_task/features/transactions/data/models/transaction_model.dart';

abstract interface class IRemoteTransactionsDataSource {
  Future<List<TransactionModel>> fetchTransactions();

  Future<bool> removeTransactions(String number);
}
