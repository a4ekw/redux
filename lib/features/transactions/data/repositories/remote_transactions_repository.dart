import 'package:test_task/features/transactions/data/data_sources/remote/i_remote_transactions_data_source.dart';
import 'package:test_task/features/transactions/data/mappers/transaction_mapper.dart';
import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';
import 'package:test_task/features/transactions/domain/repositories/i_remote_transactions_repository.dart';

final class RemoteTransactionsRepository implements IRemoteTransactionsRepository {
  final IRemoteTransactionsDataSource _remoteDataSource;

  RemoteTransactionsRepository(this._remoteDataSource);

  @override
  Future<List<TransactionEntity>> fetchTransactions() async {
    final list = await _remoteDataSource.fetchTransactions();
    return list.map((model) => TransactionMapper.toEntity(model)).toList();
  }

  @override
  Future<bool> removeTransactions(String number) => _remoteDataSource.removeTransactions(number);
}
