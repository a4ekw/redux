import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';

final class DiagramDataModel {
  final DiagramDataItemModel transfer;
  final DiagramDataItemModel deposit;
  final DiagramDataItemModel withdrawal;

  const DiagramDataModel({
    required this.transfer,
    required this.deposit,
    required this.withdrawal,
  });

  factory DiagramDataModel.empty() => DiagramDataModel(
        transfer: DiagramDataItemModel(type: TransactionType.transfer),
        deposit: DiagramDataItemModel(type: TransactionType.deposit),
        withdrawal: DiagramDataItemModel(type: TransactionType.withdrawal),
      );
}

final class DiagramDataItemModel {
  final TransactionType type;
  final double percent;

  DiagramDataItemModel({required this.type, this.percent = .0});
}
