import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:test_task/core/l10n/generated/s.dart';
import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_actions.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_state.dart';

class TransactionsList extends StatelessWidget {
  const TransactionsList({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreBuilder<TransactionsState>(
      builder: (_, store) => store.state.transactions.isEmpty
          ? Center(child: Text(S.of(context).transactionsEmpty))
          : ListView.builder(
              itemCount: store.state.transactions.length,
              itemBuilder: (_, index) {
                final entity = store.state.transactions[index];

                return _buildItem(
                  context: context,
                  entity: entity,
                  removing: store.state.removable.contains(entity),
                  onRemove: () => store.dispatch(RemoveTransactionAction(entity)),
                );
              },
            ),
    );
  }

  Widget _buildItem({
    required BuildContext context,
    required TransactionEntity entity,
    required bool removing,
    required VoidCallback onRemove,
  }) =>
      ExpansionTile(
        key: ValueKey(entity),
        title: Row(
          children: [
            entity.type.icon,
            Text(
              entity.amount.toString(),
              style: TextStyle(
                fontSize: 20.0,
                color: entity.type.color,
              ),
            ),
          ],
        ),
        subtitle: Text('${S.of(context).transactionsNumber}: ${entity.number}'),
        children: [
          _buildInfoItem(
            name: S.of(context).transactionsDate,
            value: entity.date.toString(),
          ),
          _buildInfoItem(
            name: S.of(context).transactionsAmount,
            value: entity.amount.toString(),
          ),
          _buildInfoItem(
            name: S.of(context).transactionsFee,
            value: '-${entity.fee}',
          ),
          _buildInfoItem(
            name: S.of(context).transactionsTotal,
            value: entity.total.toString(),
          ),
          _buildInfoItem(
            name: S.of(context).transactionsType,
            value: entity.type.getName(context),
          ),
          removing
              ? Container(
                  margin: const EdgeInsets.all(4.0),
                  height: 40.0,
                  width: 40.0,
                  child: const CircularProgressIndicator(),
                )
              : IconButton(
                  icon: const Icon(Icons.delete_forever, color: Colors.red),
                  onPressed: onRemove,
                ),
          const SizedBox(height: 20.0),
        ],
      );

  Widget _buildInfoItem({
    required String name,
    required String value,
  }) =>
      ListTile(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('$name: '),
            Text(value),
          ],
        ),
      );
}
