import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:test_task/core/l10n/generated/s.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_actions.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_state.dart';

class TransactionsBottomNavBar extends StatelessWidget {
  const TransactionsBottomNavBar({super.key});

  @override
  Widget build(BuildContext context) {
    final items = [
      BottomNavigationBarItem(
        icon: const Icon(Icons.list),
        label: S.of(context).transactionsBNBListTitle,
      ),
      BottomNavigationBarItem(
        icon: const Icon(Icons.data_saver_off),
        label: S.of(context).transactionsBNBDiagramTitle,
      ),
    ];
    return StoreBuilder<TransactionsState>(
      builder: (_, store) => BottomNavigationBar(
        currentIndex: store.state.tab.index,
        onTap: (index) {
          final action = index == 0 ? ToTransactionsListAction() : ToTransactionsDiagramAction();
          store.dispatch(action);
        },
        items: items,
      ),
    );
  }
}
