import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_state.dart';
import 'package:test_task/features/transactions/presentation/models/diagram_data_model.dart';

class TransactionsDiagram extends StatelessWidget {
  const TransactionsDiagram({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(50.0),
      child: StoreConnector<TransactionsState, DiagramDataModel>(
          converter: (Store<TransactionsState> store) => _createDataModel(store.state.transactions),
          builder: (_, data) {
            return Column(
              children: [
                Expanded(
                  child: PieChart(
                    PieChartData(
                      sections: [
                        PieChartSectionData(
                          value: data.transfer.percent,
                          color: data.transfer.type.color,
                          showTitle: false,
                        ),
                        PieChartSectionData(
                          value: data.deposit.percent,
                          color: data.deposit.type.color,
                          showTitle: false,
                        ),
                        PieChartSectionData(
                          value: data.withdrawal.percent,
                          color: data.withdrawal.type.color,
                          showTitle: false,
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _buildHistoryItem(context: context, model: data.deposit),
                    _buildHistoryItem(context: context, model: data.transfer),
                    _buildHistoryItem(context: context, model: data.withdrawal),
                  ],
                ),
              ],
            );
          }),
    );
  }

  Widget _buildHistoryItem({
    required BuildContext context,
    required DiagramDataItemModel model,
  }) =>
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: const EdgeInsets.all(10.0),
            height: 20.0,
            width: 20.0,
            color: model.type.color,
          ),
          Text(
            '${model.type.getName(context)} - ${model.percent.toStringAsFixed(2)}%',
          )
        ],
      );

  DiagramDataModel _createDataModel(List<TransactionEntity> list) {
    if (list.isEmpty) {
      return DiagramDataModel(
        transfer: DiagramDataItemModel(type: TransactionType.transfer),
        deposit: DiagramDataItemModel(type: TransactionType.deposit),
        withdrawal: DiagramDataItemModel(type: TransactionType.withdrawal),
      );
    }
    final transferPercent = _getPercentByType(type: TransactionType.transfer, list: list);
    final depositPercent = _getPercentByType(type: TransactionType.deposit, list: list);
    final withdrawalPercent = _getPercentByType(type: TransactionType.withdrawal, list: list);

    return DiagramDataModel(
      transfer: DiagramDataItemModel(
        type: TransactionType.transfer,
        percent: transferPercent,
      ),
      deposit: DiagramDataItemModel(
        type: TransactionType.deposit,
        percent: depositPercent,
      ),
      withdrawal: DiagramDataItemModel(
        type: TransactionType.withdrawal,
        percent: withdrawalPercent,
      ),
    );
  }

  double _getPercentByType({
    required TransactionType type,
    required List<TransactionEntity> list,
  }) {
    final totalCount = list.where((e) => e.type != TransactionType.unknown).length;
    final count = list.where((e) => e.type == type).length;
    final percent = count * 100.0 / totalCount;
    return percent;
  }
}
