import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_middleware.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_reducers.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_state.dart';

Store<TransactionsState> createTransactionsStore() => Store<TransactionsState>(
      transactionsStateReducer,
      initialState: TransactionsState.empty(),
      middleware: [
        GetIt.instance.get<TransactionsMiddleware>(),
      ],
    );
