import 'package:redux/redux.dart';
import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_actions.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_state.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_tab.dart';

TransactionsState transactionsStateReducer(TransactionsState state, action) => TransactionsState(
      isLoading: _loadingReducer(state.isLoading, action),
      tab: _tabsReducer(state.tab, action),
      transactions: _transactionsReducer(state.transactions, action),
      removable: _removableReducer(state.removable, action),
      error: _errorReducer(state.error, action),
    );

/// Loading
final _loadingReducer = combineReducers<bool>([
  TypedReducer<bool, FetchTransactionsAction>(_setLoading),
  TypedReducer<bool, FetchedTransactionsAction>(_setLoading),
  TypedReducer<bool, FetchTransactionsFailureAction>(_setLoading),
]);

bool _setLoading(_, ITransactionsActions action) {
  switch (action) {
    case FetchTransactionsAction():
      return true;
    default:
      return false;
  }
}

/// Tabs
final _tabsReducer = combineReducers<TransactionsTab>([
  TypedReducer<TransactionsTab, ToTransactionsListAction>(_setTab),
  TypedReducer<TransactionsTab, ToTransactionsDiagramAction>(_setTab),
]);

TransactionsTab _setTab(TransactionsTab tab, ITransactionsActions action) {
  switch (action) {
    case ToTransactionsDiagramAction():
      return TransactionsTab.diagram;
    default:
      return TransactionsTab.list;
  }
}

/// Transactions
final _transactionsReducer = combineReducers<List<TransactionEntity>>([
  TypedReducer<List<TransactionEntity>, FetchedTransactionsAction>(_setTransactions),
  TypedReducer<List<TransactionEntity>, TransactionRemovedAction>(_setTransactions),
]);

List<TransactionEntity> _setTransactions(
  List<TransactionEntity> list,
  ITransactionsActions action,
) {
  switch (action) {
    case FetchedTransactionsAction(transactions: final transactions):
      return transactions;
    case TransactionRemovedAction(transaction: final transaction):
      return list.toList()..remove(transaction);
    default:
      return list;
  }
}

/// Removable
final _removableReducer = combineReducers<List<TransactionEntity>>([
  TypedReducer<List<TransactionEntity>, RemoveTransactionAction>(_setRemovable),
  TypedReducer<List<TransactionEntity>, TransactionRemovedAction>(_setRemovable),
  TypedReducer<List<TransactionEntity>, RemoveTransactionFailureAction>(_setRemovable),
]);

List<TransactionEntity> _setRemovable(
  List<TransactionEntity> list,
  ITransactionsActions action,
) {
  switch (action) {
    case RemoveTransactionAction(transaction: final transaction):
      return [...list, transaction];
    case TransactionRemovedAction(transaction: final transaction):
    case RemoveTransactionFailureAction(transaction: final transaction):
      return list.toList()..remove(transaction);
    default:
      return list;
  }
}

/// Error
final _errorReducer = combineReducers<Object?>([
  TypedReducer<Object?, FetchTransactionsAction>(_setError),
  TypedReducer<Object?, FetchedTransactionsAction>(_setError),
  TypedReducer<Object?, FetchTransactionsFailureAction>(_setError),
]);

Object? _setError(Object? error, ITransactionsActions action) {
  switch (action) {
    case FetchTransactionsAction():
    case FetchedTransactionsAction():
      return null;
    case FetchTransactionsFailureAction(error: final newError):
      return newError;
    default:
      return error;
  }
}
