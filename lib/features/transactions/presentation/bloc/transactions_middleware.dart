import 'package:redux/redux.dart';
import 'package:test_task/features/transactions/domain/use_cases/fetch_transactions_use_case.dart';
import 'package:test_task/features/transactions/domain/use_cases/remove_transactions_use_case.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_actions.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_state.dart';

final class TransactionsMiddleware extends MiddlewareClass<TransactionsState> {
  final FetchTransactionUseCase _fetchTransactionUseCase;
  final RemoveTransactionUseCase _removeTransactionUseCase;

  TransactionsMiddleware(this._fetchTransactionUseCase, this._removeTransactionUseCase);

  @override
  call(Store<TransactionsState> store, action, NextDispatcher next) {
    switch (action as ITransactionsActions) {
      case FetchTransactionsAction():
        _fetchTransactions(store);
        break;
      case RemoveTransactionAction():
        _removeTransactions(store, action as RemoveTransactionAction);
        break;
      case ToTransactionsListAction():
      case ToTransactionsDiagramAction():
      case FetchedTransactionsAction():
      case FetchTransactionsFailureAction():
      case TransactionRemovedAction():
      case RemoveTransactionFailureAction():
      // TODO: Handle this.
    }
    next(action);
  }

  Future<void> _fetchTransactions(Store<TransactionsState> store) async {
    try {
      final list = await _fetchTransactionUseCase.execute();
      store.dispatch(FetchedTransactionsAction(list));
    } catch (error) {
      store.dispatch(FetchTransactionsFailureAction(error));
    }
  }

  Future<void> _removeTransactions(
    Store<TransactionsState> store,
    RemoveTransactionAction action,
  ) async {
    try {
      final result = await _removeTransactionUseCase.execute(action.transaction.number);
      if (result) {
        store.dispatch(TransactionRemovedAction(action.transaction));
        return;
      }
    } catch (_) {}
    store.dispatch(RemoveTransactionFailureAction(action.transaction));
  }
}
