import 'package:equatable/equatable.dart';
import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_tab.dart';

final class TransactionsState extends Equatable {
  final bool isLoading;
  final TransactionsTab tab;
  final List<TransactionEntity> transactions;
  final List<TransactionEntity> removable;
  final Object? error;

  const TransactionsState({
    required this.isLoading,
    required this.tab,
    required this.transactions,
    required this.removable,
    required this.error,
  });

  factory TransactionsState.empty() => const TransactionsState(
        isLoading: false,
        tab: TransactionsTab.list,
        transactions: [],
        removable: [],
        error: Null,
      );

  @override
  List<Object?> get props => [isLoading, tab, transactions, removable, error];
}
