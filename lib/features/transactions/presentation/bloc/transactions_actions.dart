import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';

sealed class ITransactionsActions {}

final class ToTransactionsListAction extends ITransactionsActions {}

final class ToTransactionsDiagramAction extends ITransactionsActions {}

final class FetchTransactionsAction extends ITransactionsActions {}

final class FetchedTransactionsAction extends ITransactionsActions {
  final List<TransactionEntity> transactions;

  FetchedTransactionsAction(this.transactions);
}

final class FetchTransactionsFailureAction extends ITransactionsActions {
  final Object error;

  FetchTransactionsFailureAction(this.error);
}

final class RemoveTransactionAction extends ITransactionsActions {
  final TransactionEntity transaction;

  RemoveTransactionAction(this.transaction);
}

final class TransactionRemovedAction extends ITransactionsActions {
  final TransactionEntity transaction;

  TransactionRemovedAction(this.transaction);
}

final class RemoveTransactionFailureAction extends ITransactionsActions {
  final TransactionEntity transaction;

  RemoveTransactionFailureAction(this.transaction);
}
