import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';
import 'package:test_task/core/l10n/generated/s.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_actions.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_state.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_tab.dart';
import 'package:test_task/features/transactions/presentation/components/transactions_bottom_nav_bar.dart';
import 'package:test_task/features/transactions/presentation/components/transactions_diagram.dart';
import 'package:test_task/features/transactions/presentation/components/transactions_list.dart';

class TransactionsScreen extends StatefulWidget {
  static const String route = '/transactions';

  const TransactionsScreen({super.key});

  @override
  State<TransactionsScreen> createState() => _TransactionsScreenState();
}

class _TransactionsScreenState extends State<TransactionsScreen> {
  final Store<TransactionsState> _store = GetIt.instance.get();

  @override
  void initState() {
    _store.dispatch(FetchTransactionsAction());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<TransactionsState>(
      store: _store,
      child: Scaffold(
        appBar: AppBar(
          title: StoreConnector<TransactionsState, String>(
            converter: (Store<TransactionsState> store) {
              switch (store.state.tab) {
                case TransactionsTab.list:
                  final count = store.state.transactions.length;
                  return '${S.of(context).transactionsListTitle} '
                      '${count == 0 ? '' : '($count)'}';
                case TransactionsTab.diagram:
                  return S.of(context).transactionsDiagramTitle;
              }
            },
            builder: (_, title) => Text(title),
          ),
        ),
        body: StoreBuilder<TransactionsState>(builder: (context, store) {
          if (store.state.isLoading) {
            return const Center(child: CircularProgressIndicator());
          }
          if (store.state.error != null) {
            return Center(
              child: Text(
                store.state.error.toString(),
                style: const TextStyle(
                  fontSize: 20.0,
                  color: Colors.red,
                ),
              ),
            );
          }
          if (store.state.tab == TransactionsTab.list || store.state.transactions.isEmpty) {
            return const TransactionsList();
          }
          return const TransactionsDiagram();
        }),
        bottomNavigationBar: const TransactionsBottomNavBar(),
      ),
    );
  }
}
