
import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';

abstract interface class IRemoteTransactionsRepository {
  Future<List<TransactionEntity>> fetchTransactions();

  Future<bool> removeTransactions(String number);
}
