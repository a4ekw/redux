import 'package:test_task/core/interfaces/i_use_case.dart';
import 'package:test_task/features/transactions/domain/repositories/i_remote_transactions_repository.dart';

final class RemoveTransactionUseCase implements IUseCaseWithParams<String, Future<bool>> {
  final IRemoteTransactionsRepository _remoteRepository;

  RemoveTransactionUseCase(this._remoteRepository);

  @override
  Future<bool> execute(String number) => _remoteRepository.removeTransactions(number);
}
