import 'package:test_task/core/interfaces/i_use_case.dart';
import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';
import 'package:test_task/features/transactions/domain/repositories/i_remote_transactions_repository.dart';

final class FetchTransactionUseCase implements IUseCase<Future<List<TransactionEntity>>> {
  final IRemoteTransactionsRepository _remoteRepository;

  FetchTransactionUseCase(this._remoteRepository);

  @override
  Future<List<TransactionEntity>> execute() => _remoteRepository.fetchTransactions();
}
