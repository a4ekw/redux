import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:test_task/core/l10n/generated/s.dart';

final class TransactionEntity extends Equatable {
  final TransactionType type;
  final DateTime date;
  final String number;
  final double amount;
  final double fee;
  final double total;

  const TransactionEntity({
    required this.type,
    required this.date,
    required this.number,
    required this.amount,
    required this.fee,
    required this.total,
  });

  @override
  List<Object?> get props => [type, number];
}

enum TransactionType {
  deposit,
  transfer,
  withdrawal,
  unknown;

  Color get color {
    switch (this) {
      case TransactionType.deposit:
        return Colors.green;
      case TransactionType.transfer:
        return Colors.amber;
      case TransactionType.withdrawal:
        return Colors.blueAccent;
      case TransactionType.unknown:
        return Colors.red;
    }
  }

  String getName(BuildContext context) {
    switch (this) {
      case TransactionType.deposit:
        return S.of(context).transactionsTypeDeposit;
      case TransactionType.transfer:
        return S.of(context).transactionsTypeTransfer;
      case TransactionType.withdrawal:
        return S.of(context).transactionsTypeWithdrawal;
      case TransactionType.unknown:
        return S.of(context).transactionsTypeUnknown;
    }
  }

  Icon get icon {
    switch (this) {
      case TransactionType.deposit:
        return Icon(
          Icons.add,
          color: TransactionType.deposit.color,
        );
      case TransactionType.transfer:
        return Icon(
          Icons.arrow_back_outlined,
          color: TransactionType.transfer.color,
        );
      case TransactionType.withdrawal:
        return Icon(
          Icons.remove,
          color: TransactionType.withdrawal.color,
        );
      case TransactionType.unknown:
        return Icon(
          Icons.error_outline,
          color: TransactionType.unknown.color,
        );
    }
  }
}
