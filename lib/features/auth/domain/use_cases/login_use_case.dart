import 'package:test_task/core/interfaces/i_use_case.dart';
import 'package:test_task/features/auth/domain/repositories/i_remote_auth_repository.dart';

final class LoginUseCase implements IUseCaseWithParams<LoginUseCaseParams, Future<bool>> {
  final IRemoteAuthRepository _remoteRepository;

  LoginUseCase(this._remoteRepository);

  @override
  Future<bool> execute(LoginUseCaseParams params) =>
      _remoteRepository.login(params.login, params.pass);
}

class LoginUseCaseParams {
  final String login;
  final String pass;

  LoginUseCaseParams({required this.login, required this.pass});
}
