
abstract interface class IRemoteAuthRepository {
  Future<bool> login(String login, String pass);
}
