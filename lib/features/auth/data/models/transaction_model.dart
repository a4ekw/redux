final class TransactionModel {
  final String type;
  final String date;
  final String number;
  final double amount;
  final double fee;
  final double total;

  TransactionModel({
    required this.type,
    required this.date,
    required this.number,
    required this.amount,
    required this.fee,
    required this.total,
  });

  factory TransactionModel.fromJson(Map<String, dynamic> json) => TransactionModel(
        type: json['type'],
        date: json['date'],
        number: json['number'],
        amount: double.parse(json['amount']),
        fee: json['fee'],
        total: json['total'],
      );
}
