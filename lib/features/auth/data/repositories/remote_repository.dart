import 'package:test_task/features/auth/data/data_sources/remote/i_remote_auth_data_source.dart';
import 'package:test_task/features/auth/domain/repositories/i_remote_auth_repository.dart';

final class RemoteAuthRepository implements IRemoteAuthRepository {
  final IRemoteAuthDataSource _remoteDataSource;

  RemoteAuthRepository(this._remoteDataSource);

  @override
  Future<bool> login(String login, String pass) => _remoteDataSource.login(login, pass);
}
