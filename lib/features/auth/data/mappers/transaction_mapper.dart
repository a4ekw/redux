import 'package:intl/intl.dart';
import 'package:test_task/core/utils/string_util.dart';
import 'package:test_task/features/transactions/data/models/transaction_model.dart';
import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';

abstract final class TransactionMapper {
  static final _dateFormat = DateFormat("dd.MM.yyyy");

  static TransactionEntity toEntity(TransactionModel model) => TransactionEntity(
        type: StringUtil.fromString(model.type),
        date: _dateFormat.parse(model.date),
        number: model.number,
        amount: model.amount,
        fee: model.fee,
        total: model.total,
      );
}
