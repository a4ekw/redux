import 'package:test_task/features/auth/data/data_sources/remote/i_remote_auth_data_source.dart';

final class MockRemoteAuthDataSource implements IRemoteAuthDataSource {
  @override
  Future<bool> login(String login, String pass) async {
    await Future.delayed(const Duration(milliseconds: 1500));
    return login == '1111' && pass == '1111';
  }
}
