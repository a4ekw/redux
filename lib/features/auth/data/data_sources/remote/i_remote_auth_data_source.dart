abstract interface class IRemoteAuthDataSource {
  Future<bool> login(String login, String pass);
}
