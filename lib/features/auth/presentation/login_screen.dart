import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:get_it/get_it.dart';
import 'package:test_task/core/l10n/generated/s.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_actions.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_state.dart';
import 'package:test_task/features/transactions/presentation/transactions_screen.dart';
import 'package:test_task/navigation_redux.dart';

class LoginScreen extends StatefulWidget {
  static const String route = '/login';

  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  final _loginController = TextEditingController();
  final _passController = TextEditingController();

  @override
  void dispose() {
    _loginController.dispose();
    _passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(S.of(context).authLoginTitle)),
      body: StoreProvider<AuthState>(
        store: GetIt.instance.get(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  controller: _loginController,
                  decoration: const InputDecoration(
                    hintText: '1111',
                    hintStyle: TextStyle(color: Colors.grey),
                  ),
                  validator: (value) {
                    if (value == null) return null;
                    if (value.isEmpty) return S.of(context).emptyField;
                    if (value.length < 4) return S.of(context).authLoginTooShort;
                    return null;
                  },
                ),
                TextFormField(
                  controller: _passController,
                  decoration: const InputDecoration(
                    hintText: '1111',
                    hintStyle: TextStyle(color: Colors.grey),
                  ),
                  validator: (value) {
                    if (value == null) return null;
                    if (value.isEmpty) return S.of(context).emptyField;
                    if (value.length < 4) return S.of(context).authPassTooShort;
                    return null;
                  },
                ),
                StoreBuilder<AuthState>(
                  onDidChange: (_, store) {
                    if (store.state.isAuth) {
                      StoreProvider.of<AppState>(context).dispatch(
                        NavigateToAction.replace(TransactionsScreen.route),
                      );
                    }
                  },
                  builder: (BuildContext context, store) => Padding(
                    padding: const EdgeInsets.only(top: 100.0),
                    child: Column(
                      children: [
                        store.state.isVerifying
                            ? const CircularProgressIndicator()
                            : ElevatedButton(
                                onPressed: () {
                                  if (_formKey.currentState?.validate() ?? false) {
                                    store.dispatch(
                                      AuthLoginAction(
                                        login: _loginController.text,
                                        pass: _passController.text,
                                      ),
                                    );
                                  }
                                },
                                child: Text(S.of(context).authLoginButtonTitle),
                              ),
                        if (store.state.isInvalid)
                          Text(
                            S.of(context).authLoginInvalidData,
                            style: const TextStyle(
                              fontSize: 20.0,
                              color: Colors.red,
                            ),
                          ),
                        if (store.state.error != null)
                          Text(
                            store.state.error.toString(),
                            style: const TextStyle(
                              fontSize: 20.0,
                              color: Colors.red,
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
