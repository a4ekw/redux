import 'package:redux/redux.dart';
import 'package:test_task/features/auth/domain/use_cases/login_use_case.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_actions.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_state.dart';

final class AuthMiddleware extends MiddlewareClass<AuthState> {
  final LoginUseCase _loginUseCase;

  AuthMiddleware(this._loginUseCase);

  @override
  call(Store<AuthState> store, action, NextDispatcher next) {
    switch (action as IAuthActions) {
      case AuthLoginAction(login: final login, pass: final pass):
        _login(store, LoginUseCaseParams(login: login, pass: pass));
        break;
      case AuthenticatedLoginAction():
      case AuthLoginFailureAction():
      case AuthLoginInvalidDataAction():
      case AuthLogoutAction():
    }
    next(action);
  }

  Future<void> _login(Store<AuthState> store, LoginUseCaseParams params) async {
    try {
      final result = await _loginUseCase.execute(params);
      if (result) return store.dispatch(AuthenticatedLoginAction());
      return store.dispatch(AuthLoginInvalidDataAction());
    } catch (error) {
      store.dispatch(AuthLoginFailureAction(error));
    }
  }
}
