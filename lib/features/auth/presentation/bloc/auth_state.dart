import 'package:equatable/equatable.dart';

final class AuthState extends Equatable {
  final bool isAuth;
  final bool isInvalid;
  final bool isVerifying;
  final Object? error;

  const AuthState({
    required this.isAuth,
    required this.isInvalid,
    required this.isVerifying,
    required this.error,
  });

  factory AuthState.initial() => const AuthState(
        isAuth: false,
        isInvalid: false,
        isVerifying: false,
        error: null,
      );

  @override
  List<Object?> get props => [isVerifying, isAuth, error];
}
