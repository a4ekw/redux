sealed class IAuthActions {}

final class AuthLoginAction extends IAuthActions {
  final String login;
  final String pass;

  AuthLoginAction({required this.login, required this.pass});
}

final class AuthenticatedLoginAction extends IAuthActions {}

final class AuthLoginFailureAction extends IAuthActions {
  final Object error;

  AuthLoginFailureAction(this.error);
}

final class AuthLoginInvalidDataAction extends IAuthActions {}

final class AuthLogoutAction extends IAuthActions {}
