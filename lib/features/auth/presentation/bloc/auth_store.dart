import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_middleware.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_reducers.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_state.dart';

Store<AuthState> createAuthStore() => Store<AuthState>(
      authStateReducer,
      initialState: AuthState.initial(),
      middleware: [
        GetIt.instance.get<AuthMiddleware>(),
      ],
    );
