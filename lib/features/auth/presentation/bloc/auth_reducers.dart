import 'package:redux/redux.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_actions.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_state.dart';

AuthState authStateReducer(AuthState state, action) => AuthState(
  isAuth: _isAuthReducer(state.isAuth, action),
  isInvalid: _isInvalidReducer(state.isInvalid, action),
  isVerifying: _isVerifyingReducer(state.isVerifying, action),
  error: _errorReducer(state.error, action),
);

/// isAuth
final _isAuthReducer = combineReducers<bool>([
  TypedReducer<bool, AuthenticatedLoginAction>(_setIaAuth),
  TypedReducer<bool, AuthLogoutAction>(_setIaAuth),
]);

bool _setIaAuth(_, IAuthActions action) {
  switch (action) {
    case AuthenticatedLoginAction():
      return true;
    default:
      return false;
  }
}

/// isInvalid
final _isInvalidReducer = combineReducers<bool>([
  TypedReducer<bool, AuthLoginAction>(_setIsInvalid),
  TypedReducer<bool, AuthLoginInvalidDataAction>(_setIsInvalid),
]);

bool _setIsInvalid(_, IAuthActions action) {
  switch (action) {
    case AuthLoginInvalidDataAction():
      return true;
    default:
      return false;
  }
}

/// isVerifying
final _isVerifyingReducer = combineReducers<bool>([
  TypedReducer<bool, AuthLoginAction>(_setIsVerifying),
  TypedReducer<bool, AuthenticatedLoginAction>(_setIsVerifying),
  TypedReducer<bool, AuthLoginFailureAction>(_setIsVerifying),
  TypedReducer<bool, AuthLoginInvalidDataAction>(_setIsVerifying),
]);

bool _setIsVerifying(_, IAuthActions action) {
  switch (action) {
    case AuthLoginAction():
      return true;
    default:
      return false;
  }
}

/// Error
final _errorReducer = combineReducers<Object?>([
  TypedReducer<Object?, AuthLoginAction>(_setError),
  TypedReducer<Object?, AuthLoginFailureAction>(_setError),
]);

Object? _setError(Object? error, IAuthActions action) {
  switch (action) {
    case AuthLoginAction():
      return null;
    case AuthLoginFailureAction(error: final newError):
      return newError;
    default:
      return error;
  }
}