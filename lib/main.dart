import 'dart:async';

import 'package:flutter/material.dart';
import 'package:test_task/app.dart';
import 'package:test_task/core/di/injections.dart';

void main() {
  runZonedGuarded(() {
    WidgetsFlutterBinding.ensureInitialized();
    Injections.init();
    runApp(const App());
  }, (error, stack) {
    debugPrint('$error\n$stack');
  });
}
