import 'package:test_task/features/transactions/domain/entities/transaction_entity.dart';

abstract final class StringUtil {
  static TransactionType fromString(String value) {
    switch (value) {
      case 'transfer':
        return TransactionType.transfer;
      case 'deposit':
        return TransactionType.deposit;
      case 'withdrawal':
        return TransactionType.withdrawal;
      default:
        return TransactionType.unknown;
    }
  }
}
