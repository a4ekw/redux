import 'package:flutter/material.dart';

abstract final class ColorUtil {
  static Color getOppositeColor(Color color) => Color.fromARGB(
        255,
        255 - color.red,
        255 - color.green,
        255 - color.blue,
      );
}
