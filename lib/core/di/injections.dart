import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';
import 'package:test_task/features/auth/data/data_sources/mock/mock_remote_data_source.dart';
import 'package:test_task/features/auth/data/data_sources/remote/i_remote_auth_data_source.dart';
import 'package:test_task/features/auth/data/repositories/remote_repository.dart';
import 'package:test_task/features/auth/domain/repositories/i_remote_auth_repository.dart';
import 'package:test_task/features/auth/domain/use_cases/login_use_case.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_middleware.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_state.dart';
import 'package:test_task/features/auth/presentation/bloc/auth_store.dart';
import 'package:test_task/features/transactions/data/data_sources/mock/mock_remote_transactions_data_source.dart';
import 'package:test_task/features/transactions/data/data_sources/remote/i_remote_transactions_data_source.dart';
import 'package:test_task/features/transactions/data/repositories/remote_transactions_repository.dart';
import 'package:test_task/features/transactions/domain/repositories/i_remote_transactions_repository.dart';
import 'package:test_task/features/transactions/domain/use_cases/fetch_transactions_use_case.dart';
import 'package:test_task/features/transactions/domain/use_cases/remove_transactions_use_case.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_middleware.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_state.dart';
import 'package:test_task/features/transactions/presentation/bloc/transactions_store.dart';
import 'package:test_task/navigation_redux.dart';

abstract final class Injections {
  static void init() {
    final getIt = GetIt.instance;

    getIt.registerLazySingleton<Store<AppState>>(createNavigationStore);

    /// Auth
    getIt.registerLazySingleton<IRemoteAuthDataSource>(() => MockRemoteAuthDataSource());
    getIt.registerLazySingleton<IRemoteAuthRepository>(() => RemoteAuthRepository(getIt.get()));
    getIt.registerLazySingleton(() => LoginUseCase(getIt.get()));
    getIt.registerLazySingleton(() => AuthMiddleware(getIt.get()));
    getIt.registerLazySingleton<Store<AuthState>>(createAuthStore);

    /// Transactions
    getIt.registerLazySingleton<IRemoteTransactionsDataSource>(
        () => MockRemoteTransactionsDataSource());
    getIt.registerLazySingleton<IRemoteTransactionsRepository>(
      () => RemoteTransactionsRepository(getIt.get()),
    );
    getIt.registerLazySingleton(() => FetchTransactionUseCase(getIt.get()));
    getIt.registerLazySingleton(() => RemoveTransactionUseCase(getIt.get()));
    getIt.registerLazySingleton(() => TransactionsMiddleware(getIt.get(), getIt.get()));
    getIt.registerLazySingleton<Store<TransactionsState>>(createTransactionsStore);
  }
}
