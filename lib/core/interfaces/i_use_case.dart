abstract interface class IUseCase<Out> {
  Out execute();
}

abstract interface class IUseCaseWithParams<In, Out> {
  Out execute(In params);
}
