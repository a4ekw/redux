import 's.dart';

/// The translations for English (`en`).
class SEn extends S {
  SEn([String locale = 'en']) : super(locale);

  @override
  String get authLoginButtonTitle => 'Log In';

  @override
  String get authLoginInvalidData => 'Invalid login data';

  @override
  String get authLoginTitle => 'Log In';

  @override
  String get authLoginTooShort => 'The login is too short';

  @override
  String get authPassTooShort => 'The password is too short';

  @override
  String get emptyField => 'The field cannot be empty';

  @override
  String get transactionsAmount => 'Amount';

  @override
  String get transactionsBNBListTitle => 'List';

  @override
  String get transactionsBNBDiagramTitle => 'Diagram';

  @override
  String get transactionsDate => 'Date';

  @override
  String get transactionsDiagramTitle => 'Transactions diagram';

  @override
  String get transactionsEmpty => 'No transactions available';

  @override
  String get transactionsFee => 'Fee';

  @override
  String get transactionsListTitle => 'Transactions list';

  @override
  String get transactionsNumber => 'Number';

  @override
  String get transactionsTotal => 'Total';

  @override
  String get transactionsType => 'Type';

  @override
  String get transactionsTypeDeposit => 'Deposit';

  @override
  String get transactionsTypeTransfer => 'Transfer';

  @override
  String get transactionsTypeWithdrawal => 'Withdrawal';

  @override
  String get transactionsTypeUnknown => 'Unknown';
}
