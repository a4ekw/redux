import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 's_en.dart';

/// Callers can lookup localized strings with an instance of S
/// returned by `S.of(context)`.
///
/// Applications need to include `S.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'generated/s.dart';
///
/// return MaterialApp(
///   localizationsDelegates: S.localizationsDelegates,
///   supportedLocales: S.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the S.supportedLocales
/// property.
abstract class S {
  S(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S)!;
  }

  static const LocalizationsDelegate<S> delegate = _SDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('en')
  ];

  /// No description provided for @authLoginButtonTitle.
  ///
  /// In en, this message translates to:
  /// **'Log In'**
  String get authLoginButtonTitle;

  /// No description provided for @authLoginInvalidData.
  ///
  /// In en, this message translates to:
  /// **'Invalid login data'**
  String get authLoginInvalidData;

  /// No description provided for @authLoginTitle.
  ///
  /// In en, this message translates to:
  /// **'Log In'**
  String get authLoginTitle;

  /// No description provided for @authLoginTooShort.
  ///
  /// In en, this message translates to:
  /// **'The login is too short'**
  String get authLoginTooShort;

  /// No description provided for @authPassTooShort.
  ///
  /// In en, this message translates to:
  /// **'The password is too short'**
  String get authPassTooShort;

  /// No description provided for @emptyField.
  ///
  /// In en, this message translates to:
  /// **'The field cannot be empty'**
  String get emptyField;

  /// No description provided for @transactionsAmount.
  ///
  /// In en, this message translates to:
  /// **'Amount'**
  String get transactionsAmount;

  /// No description provided for @transactionsBNBListTitle.
  ///
  /// In en, this message translates to:
  /// **'List'**
  String get transactionsBNBListTitle;

  /// No description provided for @transactionsBNBDiagramTitle.
  ///
  /// In en, this message translates to:
  /// **'Diagram'**
  String get transactionsBNBDiagramTitle;

  /// No description provided for @transactionsDate.
  ///
  /// In en, this message translates to:
  /// **'Date'**
  String get transactionsDate;

  /// No description provided for @transactionsDiagramTitle.
  ///
  /// In en, this message translates to:
  /// **'Transactions diagram'**
  String get transactionsDiagramTitle;

  /// No description provided for @transactionsEmpty.
  ///
  /// In en, this message translates to:
  /// **'No transactions available'**
  String get transactionsEmpty;

  /// No description provided for @transactionsFee.
  ///
  /// In en, this message translates to:
  /// **'Fee'**
  String get transactionsFee;

  /// No description provided for @transactionsListTitle.
  ///
  /// In en, this message translates to:
  /// **'Transactions list'**
  String get transactionsListTitle;

  /// No description provided for @transactionsNumber.
  ///
  /// In en, this message translates to:
  /// **'Number'**
  String get transactionsNumber;

  /// No description provided for @transactionsTotal.
  ///
  /// In en, this message translates to:
  /// **'Total'**
  String get transactionsTotal;

  /// No description provided for @transactionsType.
  ///
  /// In en, this message translates to:
  /// **'Type'**
  String get transactionsType;

  /// No description provided for @transactionsTypeDeposit.
  ///
  /// In en, this message translates to:
  /// **'Deposit'**
  String get transactionsTypeDeposit;

  /// No description provided for @transactionsTypeTransfer.
  ///
  /// In en, this message translates to:
  /// **'Transfer'**
  String get transactionsTypeTransfer;

  /// No description provided for @transactionsTypeWithdrawal.
  ///
  /// In en, this message translates to:
  /// **'Withdrawal'**
  String get transactionsTypeWithdrawal;

  /// No description provided for @transactionsTypeUnknown.
  ///
  /// In en, this message translates to:
  /// **'Unknown'**
  String get transactionsTypeUnknown;
}

class _SDelegate extends LocalizationsDelegate<S> {
  const _SDelegate();

  @override
  Future<S> load(Locale locale) {
    return SynchronousFuture<S>(lookupS(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['en'].contains(locale.languageCode);

  @override
  bool shouldReload(_SDelegate old) => false;
}

S lookupS(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'en': return SEn();
  }

  throw FlutterError(
    'S.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
