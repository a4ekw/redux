import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';
import 'package:test_task/core/l10n/generated/s.dart';
import 'package:test_task/features/auth/presentation/login_screen.dart';
import 'package:test_task/features/transactions/presentation/transactions_screen.dart';
import 'package:test_task/navigation_redux.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: GetIt.instance.get<Store<AppState>>(),
      child: MaterialApp(
        navigatorKey: NavigatorHolder.navigatorKey,
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        localizationsDelegates: S.localizationsDelegates,
        supportedLocales: S.supportedLocales,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) {
            switch (settings.name) {
              case '/':
              case LoginScreen.route:
                return const LoginScreen();
              case TransactionsScreen.route:
                return const TransactionsScreen();
              default:
                return const Placeholder();
            }
          },
        ),
      ),
    );
  }
}
