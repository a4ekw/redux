import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';

final class AppState {}

Store<AppState> createNavigationStore() => Store<AppState>(
      combineReducers<AppState>([]),
      initialState: AppState(),
      middleware: [
        const NavigationMiddleware<AppState>(),
      ],
    );
